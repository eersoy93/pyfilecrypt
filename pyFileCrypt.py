# Copyright (c) 2023 Erdem Ersoy (eersoy93)
#
# Licensed with MIT license. See LICENSE file for full text.

import sys
from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import base64
import os

def get_key(password: str, salt: bytes) -> bytes:
    kdf = PBKDF2HMAC(
                        algorithm=hashes.SHA256(),
                        length=32,
                        salt=salt,
                        iterations=100000
                    )
    key = base64.urlsafe_b64encode(kdf.derive(password.encode()))
    return key

def encrypt_file(input_file: str, output_file: str, password: str):
    salt = os.urandom(16)
    key = get_key(password, salt)
    cipher_suite = Fernet(key)

    with open(input_file, "rb") as file:
        file_data = file.read()

    encrypted_data = cipher_suite.encrypt(file_data)

    with open(output_file, "wb") as file:
        file.write(salt + encrypted_data)

def decrypt_file(input_file: str, output_file: str, password: str):
    with open(input_file, "rb") as file:
        encrypted_data = file.read()

    salt, encrypted_data = encrypted_data[:16], encrypted_data[16:]
    key = get_key(password, salt)
    cipher_suite = Fernet(key)

    try:
        decrypted_data = cipher_suite.decrypt(encrypted_data)
    except InvalidToken:
        print("Wrong password!")
        sys.exit(1)

    with open(output_file, "wb") as file:
        file.write(decrypted_data)

if __name__ == "__main__":
    if len(sys.argv) == 5:
        action = sys.argv[1]
        input_file = sys.argv[2]
        output_file = sys.argv[3]
        password = sys.argv[4]

        if action == "en":
            encrypt_file(input_file, output_file, password)
        elif action == "de":
            decrypt_file(input_file, output_file, password)
        else:
            print("Invalid action. Use 'en' for encryption or 'de' for decryption.")
            sys.exit(1)
    else:
        print("Invalid command-line arguments number.")
        print("Usage: pyFileCrypt.py <action> <input file> <output file> <password>")
        sys.exit(1)

    print(f"File '{input_file}' {action}ed successfully to '{output_file}'.")
