# pyFileCrypt
A very simple file encryption/decryption tool.

**NOTE:** It is not intended for productional use.

## Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

## Copyright
Copyright (c) 2023 Erdem Ersoy (eersoy93)

## License
Licensed with MIT license. See LICENSE for license text.

